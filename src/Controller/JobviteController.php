<?php

namespace Drupal\jobvite\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Routing;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\Component\Utility\Html;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller routines for page example routes.
 */
class JobviteController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'jobvite';
  }

  /*
   *
   */
  /**
   * Load multiple jobs.
   *
   * @param array $conditions
   *   Array of conditions to limit the returned jobs. Filtering on `id`,
   *   `location` and `category` are available. Filtered column are used as index
   *    and the required value(s) as value (scalar or array).
   * @param array $orderBy
   *   Order by conditions, as an array. Order field(s) as key(s), direction (ASC
   *   or DESC) as value. Field(s) can also be passed as value(s) for numeric keys
   *   (direction will then be ASC).
   * @param $limit
   *   Optionsal, limit to a certain number of results.
   *
   * @return array
   *   An array of job(s) or FALSE if no jobs are found.
   */
  public function jobvite_job_load_multiple(array $conditions = array(), array $orderBy = array(), $limit = NULL) {

//    $connection = \Drupal::database()->select;
//    $connection = \Drupal::database();
    $connection = Database::getConnection();
    $jobs = $connection->select('jobvite_listing', 'l')
      ->fields('l', ['data']);

    if (!is_null($limit) && is_numeric($limit)) {
      $jobs->range(0, $limit);
    }
    $conditions = array_intersect_key($conditions, array_flip(array(
      'id',
      'location',
      'category',
    )));
    foreach ($conditions as $field => $value) {
      $jobs->condition($field, $value);
    }
    foreach ($orderBy as $field => $direction) {
      if (is_numeric($field)) {
        $field = $direction;
        $direction = 'ASC';
      }
      $jobs->orderBy($field, $direction);
    }
    $results = $jobs->execute()->fetchAll();

    if (!empty($results)) {
      $results = array_map(function ($job) {
        $job = json_decode($job->data);
        if (($error = json_last_error()) == JSON_ERROR_NONE) {
          return $job;
        }
        else {
          \Drupal::logger('jobvite load')->notice('Error when parsing JSON from DB: @msg', array(
            '@msg' => json_last_error_msg()));

          return FALSE;
        }
      }, $results);
      return $results;
    }
    return [];
  }

  /**
   * Constructs a simple page.
   *
   * The router _controller callback, maps the path
   * 'job-listings' to this method.
   *
   * _controller callbacks return a renderable array for the content area of the
   * page. The theme system will later render and surround the content with the
   * appropriate blocks, navigation, and styling.
   */
  public function list() {
    $config = \Drupal::config('jobvite.settings');
    $page_title = $config->get('jobvite.page_title');
    $jobs = $this->jobvite_job_load_multiple();
    $render_array['jobvite_jobs_list'] = [
      '#theme' => 'jobvite_jobs_list',
      '#jobs' => $jobs,
      '#title' => Html::escape($page_title),
    ];
    return $render_array;
  }

  /**
   * A more complex _controller callback that takes arguments.
   *
   * This callback is mapped to the path
   * 'job/{first}/{second}'.
   *
   * The arguments in brackets are passed to this callback from the page URL.
   * The placeholder names "first" and "second" can have any value but should
   * match the callback method variable names; i.e. $first and $second.
   *
   * This function also demonstrates a more complex render array in the returned
   * values. Instead of rendering the HTML with theme('item_list'), content is
   * left un-rendered, and the theme function name is set using #theme. This
   * content will now be rendered as late as possible, giving more parts of the
   * system a chance to change it if necessary.
   *
   * Consult @link http://drupal.org/node/930760 Render Arrays documentation
   * @endlink for details.
   *
   * @param string $jobid
   *   A string to use, should be a number.
   * @param string $second
   *   Another string to use, should be a number.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   If the parameters are invalid.
   */
  public function details($jobid) {
    // Default settings.
    $config = \Drupal::config('jobvite.settings');
    $page_title = $config->get('jobvite.page_title');

    $job = jobvite_job_load($jobid);
    $render_array['jobvite_job'] = [
      '#theme' => 'jobvite_job',
      '#job' => $job,
      '#title' => Html::escape($page_title),
    ];
    return $render_array;
  }
  
  public function jobvite_redirect() {
    $requested_path = \Drupal::request()->getRequestUri();
    $config = \Drupal::config('jobvite.settings');
    $list_path = $config->get('jobvite.list_path');
    $my_requested_path = ltrim($requested_path, '/');
    $requested_parts = explode('/', $my_requested_path);
    $destination_path = NULL;
    $last_path_part = array_pop($requested_parts);
    if (!empty($last_path_part)) {
      $requested_parts[] = $last_path_part;
    }
    $arg_count = count($requested_parts);
    switch ($arg_count) {
      case 1:
        $destination_path = $list_path;
        break;
      case 2:
        $job = jobvite_job_load($requested_parts[1]);
        $destination_path = '/' . jobvite_page_path($job);
        break;
      case 3:
        $destination_path = $list_path;
        break;
    }

    if (!is_null($destination_path)) {
      $path = Url::fromUserInput('/' . $destination_path)->setAbsolute()->toString(TRUE)->getGeneratedUrl();
      return new TrustedRedirectResponse($path);
    }

    throw new NotFoundHttpException();
  }
}
