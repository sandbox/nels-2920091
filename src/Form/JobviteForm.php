<?php

namespace Drupal\jobvite\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class JobviteForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jobvite_form';
  }
    /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('jobvite.settings');
    $form['jobvite_api_information'] = array(
      '#markup' => $this->t("<p>This settings page allows you to configure the Jobvite module. You have a part for generic informations for the Jobvite API and an other to filter imported jobs.<br/>Please, report you to the <em>Jobvite Web Service - Version 2.5 – February 6, 2013.pdf</em> API's documentation of Jobvite for more informations.</p>"),
    );

    $form['jobvite_info'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Connection information'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );

    $form['jobvite_info']['jobvite_api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Api key'),
      '#required' => TRUE,
      '#default_value' => $config->get('jobvite.jobvite_api_key'),
      '#description' => $this->t('Your API key to access datas of your company'),
    );

    $form['jobvite_info']['jobvite_secret_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Secret key for Jobvite'),
      '#required' => TRUE,
      '#default_value' => $config->get('jobvite.jobvite_secret_key'),
      '#description' => $this->t('Your secret key to access datas of your company'),
    );

    $form['jobvite_info']['jobvite_company_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Company ID'),
      '#required' => TRUE,
      '#default_value' => $config->get('jobvite.company_id'),
      '#description' => $this->t('Your company ID in Jobvite'),
    );

    $form['jobvite_info']['jobvite_request_count'] = array(
      '#title' => $this->t('Number of job requested'),
      '#type' => 'number',
      '#min' => 1,
      '#step' => 1,
      '#default_value' => $config->get('jobvite.request_count'), // 100
      '#required' => TRUE,
      '#description' => $this->t('Number of jobs requested by query to the API.'),
    );

    $form['jobvite_info']['jobvite_cron_interval'] = array(
      '#title' => $this->t('Time interval for jobvite'),
      '#type' => 'number',
      '#min' => 1,
      '#step' => 1,
      '#default_value' => $config->get('jobvite.cron_interval'), // 3600
      '#required' => TRUE,
      '#description' => $this->t('Time in seconds between two importations. 3600 by default to launch the process every hour.'),
    );

    $form['jobvite_info']['jobvite_json_feed_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Url of the job feed.'),
      '#required' => TRUE,
      '#default_value' => $config->get('jobvite.json_feed_url'), //"https://api.jobvite.com/v1/jobFeed"),
      '#description' => $this->t('Enter url for the job feed of Jobvite.'),
    );

    $form['jobvite_info']['jobvite_path_prefix'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Prefix for the paths of jobs.'),
      '#description' => $this->t('The default path for individual job page is job/%jobvite_job. You can specify a replacement prefix of your choice for this path. Example: career'),
      '#default_value' => $config->get('jobvite.path_prefix'), // 'job'
      );

    $form['jobvite_info']['jobvite_list_path'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Path for the job list page.'),
      '#description' => $this->t('The path for job list page is jobs. You can specify a replacement here. Example: careers'),
      '#default_value' => $config->get('jobvite.list_path'), // jobs
      );

    $form['jobvite_filters'] = array(
      '#type' => 'details',
      '#title' => $this->t('Filters on the query'),
      '#open' => FALSE,
    );

    $form['jobvite_filters']['jobvite_job_type'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Job type'),
      '#default_value' => $config->get('jobvite.job_type'),
      '#description' => $this->t('Job type you want recover. Let blank if you want all types.'),
    );

    $form['jobvite_filters']['jobvite_available_to'] = array(
      '#type' => 'select',
      '#title' => $this->t('Available to'),
      '#options' => 
        array(
          'External',
          'Internal',
        ),
      '#default_value' => $config->get('jobvite.available_to'), // External
      '#description' => $this->t('Publishing settings for your jobs. External by default.'),
    );

    $form['jobvite_filters']['jobvite_job_category'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Category of job'),
      '#default_value' => $config->get('jobvite.job_category'),
      '#description' => $this->t('Filter your request on a category of job. Let blank to recover jobs of all categories.'),
    );

    $form['jobvite_filters']['jobvite_job_department'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Department'),
      '#default_value' => $config->get('jobvite.job_department'), // ''
      '#description' => $this->t('Filter your request on a specific department for jobs. Let blank to recover jobs of all department.'),
    );

    $form['jobvite_filters']['jobvite_job_location'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Location for jobs'),
      '#default_value' => $config->get('jobvite.job_location'),
      '#description' => $this->t('Location where you want recover jobs. Let blank if you want all locations.'),
    );

    $form['jobvite_filters']['jobvite_job_region'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Region for jobs'),
      '#default_value' => $config->get('jobvite.job_region'),
      '#description' => $this->t('Region where you want jobs are proposed. Let blank if you want all regions.'),
    );

    return $form;
  }
    /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('jobvite.settings');
    $config->set('jobvite.jobvite_api_key', $form_state->getValue('jobvite_api_key'));
    $config->set('jobvite.jobvite_secret_key', $form_state->getValue('jobvite_secret_key'));
    $config->set('jobvite.company_id', $form_state->getValue('jobvite_company_id'));
    $config->set('jobvite.request_count', $form_state->getValue('jobvite_request_count'));
    $config->set('jobvite.cron_interval', $form_state->getValue('jobvite_cron_interval'));
    $config->set('jobvite.json_feed_url', $form_state->getValue('jobvite_json_feed_url'));
    $config->set('jobvite.path_prefix', $form_state->getValue('jobvite_path_prefix'));
    $config->set('jobvite.list_path', $form_state->getValue('jobvite_list_path'));
    $config->set('jobvite.job_type', $form_state->getValue('jobvite_job_type'));
    $available_to_options = array(
      'External',
      'Internal',
    );
    $config->set('jobvite.available_to', $available_to_options[$form_state->getValue('jobvite_available_to')]);
    $config->set('jobvite.job_category', $form_state->getValue('jobvite_job_category'));
    $config->set('jobvite.job_department', $form_state->getValue('jobvite_job_department'));
    $config->set('jobvite.job_location', $form_state->getValue('jobvite_job_location'));
    $config->set('jobvite.job_region', $form_state->getValue('jobvite_job_region'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'jobvite.settings',
    ];
  }

}