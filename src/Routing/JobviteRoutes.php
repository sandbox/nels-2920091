<?php

/**
 * @file
 * Contains \Drupal\jobvite\Routing\JobviteRoutes.
 */
 
namespace Drupal\jobvite\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic jobvite route events.
 */
class JobviteRoutes {

  /**
   * {@inheritdoc}
   */
  public function routes() {

    $routes = [];
    $config = \Drupal::config('jobvite.settings');
    $path_prefix = $config->get('jobvite.path_prefix');
    $list_path = $config->get('jobvite.list_path');

    $routes['jobvite.short_link_details'] = new Route(
      '/' . $path_prefix .'/{jobid}',
      [
        '_controller' => '\Drupal\jobvite\Controller\JobviteController::jobvite_redirect',
        '_title' => 'Job Listings' # this should be from content
      ],
      [
        '_permission' => 'access content'
      ]
    );

    $routes['jobvite.medium_link'] = new Route(
      '/' . $path_prefix .'/{cat}/{title}',
      [
        '_controller' => '\Drupal\jobvite\Controller\JobviteController::jobvite_redirect',
        '_title' => 'Job Listings' # this should be from content
      ],
      [
        '_permission' => 'access content'
      ]
    );

    $routes['jobvite.details'] = new Route(
      '/' . $path_prefix . '/{category}/{title}/{jobid}',
      [
        '_controller' => '\Drupal\jobvite\Controller\JobviteController::details',
        '_title' => 'Job Listings' # this should be from content
      ],
      [
        '_permission' => 'access content'
      ]
    );

    $routes['jobvite.list'] = new Route(
      '/' . $list_path,
      [
        '_controller' => '\Drupal\jobvite\Controller\JobviteController::list',
        '_title' => 'Job Listings' # this should be configurable, too
      ],
      [
        '_permission' => 'access content'
      ]
    );

    $routes['jobvite.prefix'] = new Route(
      '/' . $path_prefix,
      [
        '_controller' => '\Drupal\jobvite\Controller\JobviteController::jobvite_redirect',
        '_title' => 'Job Listings' # this should be from content
      ],
      [
        '_permission' => 'access content'
      ]
    );

    return $routes;
  }
}
