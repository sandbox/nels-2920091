<?php

/**
 * @file
 * Contains \Drupal\jobvite\Plugin\Block\JobviteBlock.
 */

namespace Drupal\jobvite\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\jobvite\Controller\JobviteController;

/**
 * Provides a Jobvite block that lists .
 *
 * @Block(
 *   id = "jobvite_block",
 *   admin_label = @Translation("Jobvite block"),
 * )
 */
class JobviteBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $controller = new JobviteController();
    $config = $this->getConfiguration();
    $jobs = $controller->jobvite_job_load_multiple([], [], $config['block_jobcount']);
    $render_array['jobvite_jobs_block']['job_list'] = [
      '#theme' => 'jobvite_jobs_list',
      '#jobs' => $jobs,
      '#weight' => 2,
    ];
    
    switch ($config['block_desc_pos']){
      case 0:
        break;
      case 1:
        $render_array['jobvite_jobs_block']['description'] = [
          '#type' => 'processed_text',
          '#prefix' => '<div class="jobvite-desc">',
          '#suffix' => '</div>',
          '#text' => $config['block_desc_text'],
          '#weight' => 1,
        ];
        break;
      case 2:
        $render_array['jobvite_jobs_block']['description'] = [
          '#type' => 'processed_text',
          '#prefix' => '<div class="jobvite-desc">',
          '#suffix' => '</div>',
          '#text' => $config['block_desc_text'],
          '#weight' => 3,
        ];
        break;      
    }
    return $render_array['jobvite_jobs_block'];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['jobvite_block_jobcount'] = array(
      '#title' => $this->t('Number of Jobs to display in block'),
      '#default_value' =>  isset($config['block_jobcount']) ? $config['block_jobcount'] : 1,
      '#type' => 'number',
      '#min' => 1,
    );
    $form['jobvite_block_description'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['jobvite_block_description']['jobvite_block_desc_text'] = array(
      '#title' => $this->t('Intro text'),
      '#default_value' =>  isset($config['block_desc_text']) ? $config['block_desc_text'] : NULL,
      '#type' => 'textarea',
    );
    $form['jobvite_block_description']['jobvite_block_desc_position'] = array(
      '#title' => $this->t('Text position'),
      '#default_value' =>  isset($config['block_desc_pos']) ? $config['block_desc_pos'] : 0,
      '#type' => 'select',
      '#options' => array(
         0 => t('None'),
         1 => t('Above'),
         2 => t('Below'),
       ),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('block_jobcount', $form_state->getValue('jobvite_block_jobcount'));
    $this->setConfigurationValue('block_desc_text', $form_state->getValue(['jobvite_block_description', 'jobvite_block_desc_text']));
    $this->setConfigurationValue('block_desc_pos', $form_state->getValue(['jobvite_block_description', 'jobvite_block_desc_position']));
  }

}