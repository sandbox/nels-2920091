<?php

/**
 * @file
 * Contains \Drupal\jobvite\Plugin\QueueWorker\JobviteFetcher.
 */

namespace Drupal\jobvite\Plugin\QueueWorker;

//use Drupal\jobvite\FeedInterface;
use Drupal\Core\Queue\QueueWorkerBase;

/**
 * @QueueWorker(
 *   id = "jobvite_fetcher",
 *   title = @Translation("Jobvite Fetcher"),
 *   cron = {"time" = 60}
 * )
 */
class JobviteFetcher extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    /**
     * Each item is a separate request to the Jobvite API. The requests returns jobs
     * listing that are then added to the jobvite_queue_db queue. Following the
     * request we are able to determine if subsequent requests are needed to
     * retrieve more job listings. When all jobs have been retrieve a delete item is
     * added as the last item.
     *
     * @param array $item
     *   A work unit (ie. queue item), an array containing:
     *   - uri: url of the Jobvite feed with all the specific GET parameters
     *   - request_options: headers and context for the request
     *   - timestamp: time of last the request, used to identify older job listings
     *     to be removed.
     */

    // Make a request to the Jobvite API.
    $client = \Drupal::httpClient();
    try {
      $response = $client->request('GET', $item->uri);
      $code = $response->getStatusCode();
      if ($code == 200) {
        // Expected result.
        // getBody() returns an instance of Psr\Http\Message\StreamInterface.
        // @see http://docs.guzzlephp.org/en/latest/psr7.html#body

        $result = json_decode($response->getBody()->getContents());
        if (($error = json_last_error()) == JSON_ERROR_NONE) {
          if (isset($result->filterCriteria)) {
            $start = $result->filterCriteria->start;
          }
          else {
            $start = 0;
          }

          $total = isset($result->total) ? $result->total : 0;
          $count = 0;
          // Add all jobs to queue that handles DB merge.
          $jobresults = isset($result->jobs) ? $result->jobs : $result->requisitions; // v1 / v2
          if (!empty($jobresults)) {

            /** @var QueueFactory $queue_factory */
            $queue_factory = \Drupal::service('queue');
            /** @var QueueInterface $queue */
            $queue = $queue_factory->get('jobvite_db_worker');
            $count = count($jobresults);
            foreach ($jobresults as $job) {
              $merge_item = new \stdClass();
              $merge_item->operation = 'merge';
              $jobid = isset($job->id) ? $job->id : $job->eId; // v1 / v2
              $merge_item->id = $jobid;
              $merge_item->data = $job;
              $merge_item->timestamp = $item->timestamp;
              $queue->createItem($merge_item);
            }

            // Add another request to the queue for the remaining jobs
            // or add a delete item at the end of the DB queue.
            if (($start + $count) < $total) {
              jobvite_enqueue_request($start + $count, $item->timestamp);
            }
            else {
              $delete_item = new \stdClass();
              $delete_item->operation = 'delete';
              $delete_item->timestamp = $item->timestamp;
              $queue->createItem($delete_item);
            }
          }
        }
      }
      else {
        // Logs a notice
        \Drupal::logger('jobvite')->notice('Error when parsing JSON from Jobvite: @msg', array(
          '@msg' => $e->getMessage(),
        ));
      }
    }
    catch (RequestException $e) {
      \Drupal::logger('jobvite')->notice($e);
    }
  }
}