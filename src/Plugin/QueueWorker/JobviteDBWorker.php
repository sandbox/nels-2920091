<?php

/**
 * @file
 * Contains \Drupal\jobvite\Plugin\QueueWorker\JobviteDBWorker.
 */

namespace Drupal\jobvite\Plugin\QueueWorker;
use Drupal\Core\Queue\QueueWorkerBase;

/**
 * @QueueWorker(
 *   id = "jobvite_db_worker",
 *   title = @Translation("Jobvite DB Worker"),
 *   cron = {"time" = 60}
 * )
 */
class JobviteDBWorker extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    /**
     * Worker function for jobvite_queue_db.
     *
     * Each item can do one of two things: merge an entry to the database or delete
     * all the older job listings. This behavior is set by the operation element
     * inside the item object.
     *
     * @param object $item
     *   A work unit (ie. queue item), an object containing:
     *   - operation: merge or delete
     *   - id: the job ID (only present if the operation is set to merge)
     *   - data: the complete job data object as returned by Jobvite (only present
     *     if the operation is set to merge).
     *   - timestamp: time of last the request, used to identify older job listings
     *     to be removed.
     */
    // Add or update job listings.
    if ($item->operation == 'merge') {
      $jobid = isset($item->data->id) ? $item->data->id : $item->data->eId; // v1 / v2
      $connection = \Drupal::database();
      $connection->merge('jobvite_listing')
        ->key(array('id' => $jobid))
        ->updateFields(array(
          'id' => $jobid,
          'title' => $item->data->title,
          'location' => isset($item->data->location) ? $item->data->location : '', // v2 allows empty
          'category' => $item->data->category,
          'timestamp' => $item->timestamp,
          'data' => json_encode($item->data),
        ))
        ->execute();
    }

    // Delete job listings that are no longer in the Jobvite feed.
    if ($item->operation == 'delete') {
      $connection = \Drupal::database();
      $connection->delete('jobvite_listing')
        ->condition('timestamp', $item->timestamp, '<')
        ->execute();
    }
  }
}
